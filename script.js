let number = 2;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}`);

let address = ['256 Washington Ave NW', 'California', 90011];

const [city, country, code] = address;
console.log(`I live at ${city}, ${country} ${code}`);

let animal = {
	name: "Lolong",
	kind: 'Saltwater Crocodile',
	weight: '1075 kgs',
	lenght: '20 ft. 3 in.'
};


const {name, kind, weight, lenght} = animal;

console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${lenght}`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => console.log(num));



const sum = numbers.reduce((prev, current) => prev + current);

console.log(sum);



class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const frankie = new dog('Frankie', 5, 'Miniature Dachshund');
console.log(frankie);